local g = golly()
local gp = require "gplus"

local drawstate = g.getoption("drawingstate")

local function drawcircle(x0, y0, r)
	local x, y, e, ix, iy
	x = 0
	y = r
	e = 3 - 2 * r
	while x < y do
		ix = math.floor(x)
		iy = math.floor(y)

		g.setcell(x0 + ix, y0 + iy, drawstate)
		g.setcell(x0 - ix, y0 + iy, drawstate)
		g.setcell(x0 + ix, y0 - iy, drawstate)
		g.setcell(x0 - ix, y0 - iy, drawstate)
		g.setcell(x0 + iy, y0 + ix, drawstate)
		g.setcell(x0 - iy, y0 + ix, drawstate)
		g.setcell(x0 + iy, y0 - ix, drawstate)
		g.setcell(x0 - iy, y0 - ix, drawstate)

		if e >= 0 then
			e = e + 4 * (x - y) + 10
			x = x + 1
			y = y - 1
		else
			e = e + 4 * x + 6
			x = x + 1
		end
	end

end

local prev, x, y, r, evt, butt, mods, event
r = 0
while true do
	event = g.getevent()
	if event:find("click") == 1 then
		evt, x, y, butt, mods = gp.split(event)
		x = tonumber(x)
		y = tonumber(y)

		if prev then
			drawcircle(prev.x, prev.y, r)
			g.update()
			break
		end
		prev = {x = x, y = y}
	else 
		local mousepos = g.getxy()
		if prev then
			if #mousepos > 0 then
				local x, y = gp.split(mousepos)
				r = ((x - prev.x)^2 + (y - prev.y)^2)^0.5
				g.show("Circle radius: "..math.floor(r).." cells")
			end
		else
			g.show("Click where the circle origin should be")
		end
	end
end

