local g = golly()
local gp = require "gplus"

local drawstate = g.getoption("drawingstate")

local function drawline(x1, y1, x2, y2)
	local x, y, dx, dy, adx, ady, e
	x = x1
	y = y1
	dx = x2 - x1
	dy = y2 - y1
	adx = math.abs(dx)
	ady = math.abs(dy)
	g.setcell(x, y, drawstate)

	local step_y = 1
	local step_x = 1
	if dy < 0 then step_y = -1 end
	if dx < 0 then step_x = -1 end

	if adx > ady then
		e = 2 * dy - dx
		while x ~= x2 do
			if e >= 0 then
				x = x + step_x
				y = y + step_y
				e = e + 2 * (ady - adx)
			else
				x = x + step_x
				e = e + 2 * ady
			end

			g.setcell(x, y, drawstate)
		end
	else
		e = 2 * dx - dy
		while y ~= y2 do
			if e >= 0 then
				x = x + step_x
				y = y + step_y
				e = e + 2 * (adx - ady)
			else
				y = y + step_y
				e = e + 2 * adx
			end

			g.setcell(x, y, drawstate)
		end
	end
end

local prev, x, y, evt, butt, mods, event
while true do
	event = g.getevent()

	if event:find("click") == 1 then
		evt, x, y, butt, mods = gp.split(event)
		x = tonumber(x)
		y = tonumber(y)
		
		if prev then
			drawline(prev.x, prev.y, x, y)
			g.update()
		end
		prev = {x = x, y = y}
	end
end

